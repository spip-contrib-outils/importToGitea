#!/usr/bin/env bash

hash curl 2>/dev/null || { echo >&2 "curl is required. Stopped."; exit 1; }
hash jq 2>/dev/null || { echo >&2 "jq is required. Stopped."; exit 1; }

. ImportToGitea.conf


USERS=$(curl -sH "Authorization: token $GITEA_TOKEN" \
            -H  "accept: application/json" \
            -X GET \
            "$GITEA_HOST:$GITEA_PORT/api/v1/admin/users" |jq -r '.[] | select(.email|match("incubic.pro|ukr.net")) |.login')

for username in $USERS; do
    curl -X DELETE \
            -H "Authorization: token $GITEA_TOKEN" \
            "https://git.spip.net/api/v1/admin/users/$username"
done
exit
