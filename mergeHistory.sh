#!/bin/bash
set -euo pipefail

#Reference
#https://git-scm.com/docs/git-svn
#https://stackoverflow.com/questions/5188914/how-to-show-first-commit-by-git-log
#https://stackoverflow.com/questions/4624881/how-can-i-uncommit-the-last-commit-in-a-git-bare-repository
#https://stackoverflow.com/questions/13988511/remove-a-directory-from-all-previous-commits/13992233#13992233

. $(dirname $(readlink -f $0))"/ImportToGitea.conf"

if [[ -z "$1" ]] || [[ ! -d "$1" ]]; then
    echo "Miss subgit repository source"
    exit 1;
else
    git_public="$1"
fi

if [[ ! -f "$git_public/subgit/config" ]]; then
    echo "No subgit configuration"
    exit 1;
else
    svn_source=$(< "$git_public/subgit/config" grep url|sed 's/.*\(svn:.*\)/\1/')
fi

if [[ -z "$svn_source" ]]; then
    echo "No subgit/svn configuration"
    exit 1;
fi

gitsvn_repo=$(mktemp -d)

#Full svn export
#Set a trunk to follow all svn move/copy
git svn clone --trunk / "$svn_source/" "$gitsvn_repo"

#Remove standard layout
git -C "$gitsvn_repo" filter-branch -f --index-filter 'git rm --cached -r --ignore-unmatch trunk' --prune-empty --tag-name-filter cat -- --all
git -C "$gitsvn_repo" filter-branch -f --index-filter 'git rm --cached -r --ignore-unmatch branches' --prune-empty --tag-name-filter cat -- --all
git -C "$gitsvn_repo" filter-branch -f --index-filter 'git rm --cached -r --ignore-unmatch tags' --prune-empty --tag-name-filter cat -- --all

#Move to previous commit
#This commit should be a move to trunk directory, we don't want it
git -C "$gitsvn_repo" update-ref HEAD HEAD^

first_commit_post_trunk=$(git -C "$git_public" rev-list --max-parents=0 HEAD)
last_commit_pre_trunk=$(git -C "$gitsvn_repo" rev-list --max-count=1 HEAD)

git -C "$git_public" fetch "$gitsvn_repo" refs/heads/master:refs/old_history/pre_trunk

git -C "$git_public" replace --graft "$first_commit_post_trunk" "$last_commit_pre_trunk"