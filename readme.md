# Exporter/Importer/Synchroniser des dépots git <> svn

## Introduction

> Le dépot permet de synchroniser des projets svn de zone.spip.net vers/depuis git sur git.spip.net


## Usage : Import depuis svn (ImportOneRepository.sh)

Le script importe un dépot (-r) selon la structure définie par le modéle (-m) dans l'organisation git (-o) 

```
ImportOneRepository, a command line interface to import a svn repository to gitea  
    -r <repository> or --repository <reposoitoy>              specify the svn repository name to import  
    -o <organization> or --orgnanization <organization>       gitea organization where to import repository.
    -m <model> or --model <model>                             specify the model svn structure to respesct (dist or zone)
    -d or --delete                                            Remove gitea repository first
```

### Exemple avec un plugin de la zone

```
cd /var/git/subgit/scripts/importToGitea#
bash ImportOneRepository.sh -r saisies -o _plugins_ -m zone
```

Sur la base du modele (-m) **zone** et  du dépot (-r) **saisies**, la source svn est considérée présente à l'adresse svn://svn.spip.net/spip-zone/_plugins_/saisies
Sur l'indication de l'organisation (-o) **_plugins_** , le depot (-r) **saisies** sera importé dans git à l'adresse suivante https://git.spip.net/_plugins_/saisies


Il est possible de recréer le dépot git, par exemple suite à une modification du modèle avec l'option -d. Elle supprime le depot géré par gitea 

### Cas particulier des plugins-dist 

```
cd /var/git/subgit/scripts/importToGitea
bash dist.sh
```

Ce [script](https://git.spip.net/_outils_/importToGitea/src/branch/master/dist.sh) va importer une liste de plugins identifiés manuellement. 
Pour chacun de ces plugins, un dépot git dans l'organisation [dist](https://git.spip.net/dist/) sera créé. 
Le modele **dist** est utilisé. Il prend en compte les [différents chemins](https://git.spip.net/_outils_/importToGitea/src/branch/master/models/dist.conf#L5-L9) relatifs à trunk, tags et branches pour faire un dépot git consistant (tags selon spip ou paquet.xml).


## Usage : Export depuis git (ExportToSvn.sh)

Le script exporte le dépot (-r) présent dans l'organisation (-o) sur le svn zone.spip.net sur la base d'un modele (-m)

```
ExportToSvn, a command line interface to export a git projet to svn
    -r <repository> or --repository <reposoitoy>            specify the svn repository name to import
    -o <organization> or --orgnanization <organization>     gitea organization where to import repository.
    -m <model> or --model <model>                           specify the model svn structure to respect (dist or zone)
```

### Exemple avec le projet ImportToGitea

```
cd /var/git/subgit/scripts/importToGitea
bash ./ExportToSvn.sh -r importToGitea -o _outils_ -m outils
```

Le dépot (-r) **importToGitea** de l'organisation (-o) **\_outils\_** sera exporté sur la base du modéle (-m) **outils** sur le svn à l'adresse /\_outils\_/importToGitea

Ce qui donne :
* source : https://git.spip.net/_outils_/importToGitea/
* cible : https://zone.spip.net/trac/spip-zone/browser/spip-zone/_outils_/importToGitea

## Synchronisation bijective ?

Subgit permet de gérer la synchronisation dans les 2 sens. Toutefois il faut prendre en compte qu'il est préférable de considérer un seul dépot de référence, le second étant un miroir.
Pour les personnes ayant demandé la mise en place de cette fonctionnalité, il est **fortement** conseillé d'indiquer dans les informations du projet si celui ci priviligie git ou svn. 
Le chapitre suivant peut être pris comme base et amélioré.


## Contribuer

Les correctifs sont avec plaisir appréciés et bienvenus.
Pour contribuer il est conseillé d'utiliser la version [git](https://git.spip.net/_outils_/importToGitea/).

### Git ou SVN ?

Le code est disponible sur :
* [git](https://git.spip.net/_outils_/importToGitea/)
* [svn](https://zone.spip.net/trac/spip-zone/browser/spip-zone/_outils_/importToGitea).

Ces 2 versions sont synchronisées et par conséquent il est normalement possible de lire et écrire indifféremment sur les 2 dépots.
**Toutefois** c'est la version git qui sert de référence. 
Si pour une raison quelleconque la synchronisation est cassée, ce sera git qui aura le dernier mot.
