#!/usr/bin/env bash

hash curl 2>/dev/null || { echo >&2 "curl is required. Stopped."; exit 1; }
hash jq 2>/dev/null || { echo >&2 "jq is required. Stopped."; exit 1; }

# Manage arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -r|--repository)
    REPOSITORY="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--organization)
    ORGANIZATION="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--target)
    TARGET="$2"
    shift # past argument
    shift # past value
    ;;
    -m|--model)
    MODEL="$2"
    shift # past argument
    shift # past value
    ;;
    -d|--delete)
    DELETE=1
    shift # past argument
    ;;
    -h|--help)
    shift # past argument
    printf "ImportOneRepository, a command line interface to import a svn repository to gitea"
    printf "  -r <repository> or --repository <reposoitoy> \\t\\t specify the svn repository name to import"
	printf "  -o <organization> or --orgnanization <organization> \\t gitea organization where to import repository."
	printf "  -m <model> or --model <model> \\t\\t specify the model svn structure to respesct (dist or zone)"
	printf "  -d or --delete \\t\\t Remove gitea repository first"
	exit 0
    ;;
    --*)    # unknown option
    cli_output "Invalid option: ${1}. Type --help to show help" red notime
    shift
    exit 1
    ;;
    -*)    # unknown option
    cli_output "Invalid option: ${1}. Type --help to show help" red notime
    shift
    exit 1
    ;;
    *)    # unknown option
    FILES+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${FILES[@]}" # restore positional parameters

#Control requirement configuration
if [ ! -f ImportToGitea.conf ]; then
    echo "Gitea configuration not set, can't continue"
    exit 1
fi

. ImportToGitea.conf

#Control model
if [ ! -f "models/${MODEL}.conf" ]; then
    echo "Model is not set, can't continue"
    exit 1
fi

if [ -z "${ORGANIZATION}" ]; then
    echo "Organization missing, can't continue"
    exit 1
fi

if [ -z "${REPOSITORY}" ]; then
    echo "repository missing, can't continue"
    exit 0
fi

if [ -z "${TARGET}" ]; then
    TARGET=$(basename "$REPOSITORY")
fi

echo "$REPOSITORY will be import from svn following $MODEL to $ORGANIZATION"

#Disting svn and git repository
repository_svn="$REPOSITORY"
repository_git="$TARGET"

#Import svn to git
repo_source=$TMP_SUBGIT_DIR/$repository_svn
#Restore subgit configuration
repo_target=$GITEA_REPO/$ORGANIZATION/$repository_git.git
#LowerCase
repo_target=${repo_target,,}

mkdir -p "$repo_source"
subgit configure --svn-url svn://svn.tld "$repo_source"
cp "models/${MODEL}.conf" "$repo_source/subgit/config"
sed -i "s#__PLUGIN__#$repository_svn#" "$repo_source/subgit/config"
subgit install "$repo_source"
subgit uninstall "$repo_source"

if [ ! -z "$DELETE" ]; then
    subgit uninstall "$repo_target"
    curl -H "Authorization: token $GITEA_TOKEN" \
            -X "DELETE" \
            "$GITEA_HOST:$GITEA_PORT/api/v1/repos/$ORGANIZATION/$repository_git"
fi

#Create Gitea repository
ORGANIZATION_ID=$(curl -sH "Authorization: token $GITEA_TOKEN" \
        "$GITEA_HOST:$GITEA_PORT/api/v1/orgs/$ORGANIZATION" | jq ".id")
TEAM_ID=$(curl -sH "Authorization: token $GITEA_TOKEN" \
        "$GITEA_HOST:$GITEA_PORT/api/v1/orgs/$ORGANIZATION/teams" |jq '.[] | select(.name == "contrib") | .id ')


curl -H "Authorization: token $GITEA_TOKEN" \
        --data "clone_addr=$repo_source&uid=$ORGANIZATION_ID&repo_name=$repository_git" \
        "$GITEA_HOST:$GITEA_PORT/api/v1/repos/migrate"

curl -X PUT \
        -H "Authorization: token $GITEA_TOKEN" \
        "https://git.spip.net/api/v1/teams/$TEAM_ID/repos/$ORGANIZATION/$repository_git"

#Restore subgit configuration
cp -bar "$repo_source/"{subgit,svn,db,logs} "$repo_target/"
cd "$repo_target/" || exit 1
rename 's/(hooks\/)(.*receive).d$/$1user-$2.d/' hooks/*
subgit install "$repo_target"
sudo subgit register --key /var/git/subgit/subgit.key "$repo_target"

#Clean Directory
rm -rf "$repo_source"

sudo chown "$GIT_UID:$GIT_GID" -R "$repo_target"
