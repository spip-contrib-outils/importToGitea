#!/bin/bash
#Liste des plugins-dist
#Ils sont tous ranges dans le svn _core_/plugins
#Se reporter au modele/dist.conf pour voir la correspondance trunk/branches/tags
plugins=( aide archiviste breves compagnon compresseur dev dist dump filtres_images forum grenier jquery_ui mediabox medias mots msie_compat organiseur petitions plan porte_plume revisions safehtml sites squelettes_par_rubrique statistiques svp textwheel themes urls_etendues vertebres )

for plugin in "${plugins[@]}"; do
	echo "$plugin"
	#Importer chaque projet svn dans l'organisation dist (si le depot existe il est ecrase avant)
	bash -x ImportOneRepository.sh -d -m dist -o dist -r "$plugin"
 	#Activer la clef pour subgit
	subgit register --key /var/git/subgit/subgit.key "/var/git/gitea/spip/repositories/dist/$plugin.git"
done
