#!/bin/bash

#https://stackoverflow.com/questions/16972031/how-to-unpack-all-objects-of-a-git-repository
#https://git-scm.com/docs/git-unpack-objects
#https://git.seveas.net/repairing-and-recovering-broken-git-repositories.html

repo_to_restore="$1"
backup_dir="$2"

#backup dir could be a full workspace or only a .git content
#repo_to_restore should be a git bare directory but could be a local workspace
if [ ! -d "$backup_dir" ];then
        echo "miss backup dir"
        exit 1;
fi

if [ ! -d "$repo_to_restore" ];then
        echo "miss repo to restore"
        exit 1;
fi

if [ -d "$backup_dir/.git" ];then
        backup_dir="$backup_dir/.git"
fi

if [ -d "$repo_to_restore/.git" ];then
        backup_dir="$repo_to_restore/.git"
fi

#Find objects broken
#missing or corrupt

missings=$(git -C $repo_to_restore fsck 2>/dev/null|grep missing|cut -d " " -f 3)
corrupts=$(git -C $repo_to_restore fsck 2>&1|grep "error:.*corrupt"|cut -d " " -f 2|cut -d ":" -f 1)


## First unpack in backup directory all object
## We want restore only broken objects
for pack in "$backup_dir/objects/pack/*.pack";do
        echo "$packs"
        mv $pack $pack.bkp 
        git -C $repo_to_restore unpack-objects < $pack.bkp  
done

## If backup_dir was yet unpacked
## Maintain idempotence script
for pack in "$backup_dir/objects/pack/*.pack.bkp";do
        echo "$packs"
        git -C $repo_to_restore unpack-objects < $pack      
done


##Restore missing objects
for object in $missings; do
        echo $object
        if [ -f "$backup_dir/objects/${object:0:2}/${object:2}" ]; then
                echo -e "\t${object:0:2}${object:2}"
                mkdir "$repo_to_restore/objects/${object:0:2}"
                cp "$backup_dir/objects/${object:0:2}/${object:2}" "$repo_to_restore/objects/${object:0:2}"
        fi
done

## First unpack in backup directory all object
## We want restore only broken objects
for pack in "$backup_dir/objects/pack/*.pack";do
        echo "$packs"
        mv $pack $pack.bkp 
        git -C $repo_to_restore unpack-objects < $pack.bkp  
done

## Is backup_dir was yet unpacked
for pack in "$backup_dir/objects/pack/*.pack.bkp";do
        echo "$packs"
        git -C $repo_to_restore unpack-objects < $pack      
done


##Restore missing objects
for object in $missings; do
        echo $object
        if [ -f "$backup_dir/objects/${object:0:2}/${object:2}" ]; then
                echo -e "\t${object:0:2}${object:2}"
                mkdir "$repo_to_restore/objects/${object:0:2}"
                cp "$backup_dir/objects/${object:0:2}/${object:2}" "$repo_to_restore/objects/${object:0:2}"
        fi
done

##Restore corrupt objects
for object in $corrupts; do
        echo $object
        echo $backup_dir/objects/${object:0:2}/${object:2}
        if [ -f "$backup_dir/objects/${object:0:2}/${object:2}" ]; then
                echo -e "\t${object:0:2}${object:2}"
                mkdir "$repo_to_restore/objects/${object:0:2}"
                cp "$backup_dir/objects/${object:0:2}/${object:2}" "$repo_to_restore/objects/${object:0:2}/${object:2}"
        fi
done


chown -R git:git "$repo_to_restore"
