#!/usr/bin/env bash

hash curl 2>/dev/null || { echo >&2 "curl is required. Stopped."; exit 1; }
hash jq 2>/dev/null || { echo >&2 "jq is required. Stopped."; exit 1; }

# Manage arguments
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -r|--repository)
    REPOSITORY="$2"
    shift # past argument
    shift # past value
    ;;
    -o|--organization)
    ORGANIZATION="$2"
    shift # past argument
    shift # past value
    ;;
    -m|--model)
    MODEL="$2"
    shift # past argument
    shift # past value
    ;;
    -t|--target)
    TARGET="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    shift # past argument
    printf "ExportToSvn, a command line interface to export a git projet to svn"
    printf "  -r <repository> or --repository <reposoitoy> \\t\\t specify the svn repository name to import"
	printf "  -o <organization> or --orgnanization <organization> \\t gitea organization where to import repository."
	printf "  -m <model> or --model <model> \\t\\t specify the model svn structure to respect (dist or zone)"
	printf "  -t <root svn> or --target <root svn> \\t\\t Specify target to prefix svn path before repository name"
	exit 0
    ;;
    --*)    # unknown option
    cli_output "Invalid option: ${1}. Type --help to show help" red notime
    shift
    exit 1
    ;;
    -*)    # unknown option
    cli_output "Invalid option: ${1}. Type --help to show help" red notime
    shift
    exit 1
    ;;
    *)    # unknown option
    FILES+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${FILES[@]}" # restore positional parameters

#Control requirement configuration
if [ ! -f ImportToGitea.conf ]; then
    echo "Gitea configuration not set, can't continue"
    exit 1
fi

. ImportToGitea.conf

#Control model
if [ ! -f "models/${MODEL}.conf" ]; then
    echo "Model is not set, can't continue"
    exit 1
fi

if [ -z "${ORGANIZATION}" ]; then
    echo "Organization missing, can't continue"
    exit 1
fi

if [ -z "${REPOSITORY}" ]; then
    echo "repository missing, can't continue"
    exit 0
fi

echo "$ORGANIZATION/$REPOSITORY will be export to svn following $MODEL"


#Local git repository
repo_source=$GITEA_REPO/$ORGANIZATION/$REPOSITORY.git
#LowerCase
repo_source=${repo_source,,}

sudo -u git -s subgit configure --svn-url svn://svn.tld "$repo_source"
cp "models/${MODEL}.conf" "$repo_source/subgit/config"
sed -i "s/__PLUGIN__/$TARGET$REPOSITORY/" "$repo_source/subgit/config"
#Load gitea hooks
rename 's/(hooks\/)(.*receive).d$/$1user-$2.d/' hooks/*
sudo -u git -s subgit install "$repo_source"
subgit register --key /var/git/subgit/subgit.key "$repo_source"

#Restore subgit configuration
#cp -bar "$repo_source/"{subgit,svn,db,logs} "$repo_target/"
#cd "$repo_target/" || exit 1

#subgit install "$repo_target"

chown "$GIT_UID:$GIT_GID" -R "$repo_source"
